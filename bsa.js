import Chat from './src/components/Chat/Chat';
import rootReducer from './src/reducers/index';

export default {
    Chat,
    rootReducer,
};
