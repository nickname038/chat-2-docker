import {
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
} from "./actionTypes";
import service from "./service";

export const addMessage = (message) => ({
  type: ADD_MESSAGE,
  payload: {
    id: service.getNewId(),
    message,
  },
});

export const updateMessage = (id, text, time) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    text,
    editedAt: time,
  },
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: {
    id,
  },
});

export const likeMessage = (id) => ({
  type: LIKE_MESSAGE,
  payload: {
    id,
  },
});
