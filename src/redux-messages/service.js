const getNewId = () => new Date().getTime();

const methods = { getNewId };

export default methods;
