import { SET_CHANGE_MESSAGE_ID } from "./actionTypes";

const initialState = null;

export default function setChangeMessageIdReducer(state = initialState, action) {
  switch (action.type) {
    case SET_CHANGE_MESSAGE_ID: {
      const { id } = action.payload;
      return id;
    }

    default:
      return state;
  }
}
