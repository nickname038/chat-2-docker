import React from "react";
import "./MessageList.css";
import Message from "./Message/Message";
import OwnMessage from "./Message/OwnMessage/OwnMessage";

let currData;
let newData;
let isNewDayMessage;

class MessageList extends React.Component {
  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView();
  };

  componentDidMount() {
    this.scrollToBottom();
  }

  render() {
    const renderMessages = this.props.messages.map((message, i) => {
      newData = message.createdAt.substr(0, 10);
      isNewDayMessage = currData !== newData || i === 0;
      currData = newData;
      if (message.userId === this.props.myUserId) {
        return (
          <OwnMessage
            message={message}
            isNewDayMessage={isNewDayMessage}
            date={currData}
            onChangeMessage={this.props.onChangeMessage}
            resetChengingFlag={this.props.resetChengingFlag}
            onDeleteMessage={this.props.onDeleteMessage}
            key={message.id}
          />
        );
      }
      return (
        <Message
          message={message}
          isNewDayMessage={isNewDayMessage}
          date={currData}
          onLikeMessage={this.props.onLikeMessage}
          key={message.id}
        />
      );
    });
    return (
      <div className="message-wrapper">
        <div className="message-list">{renderMessages}</div>
        <div
          style={{ float: "left", clear: "both" }}
          ref={(el) => {
            this.messagesEnd = el;
          }}
        ></div>
      </div>
    );
  }
}

export default MessageList;
