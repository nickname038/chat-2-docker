import React from "react";
import "./Chat.css";
import Header from "./Header/Header";
import MessageInput from "./MessageInput/MessageInput";
import MessageList from "./MessageList/MessageList";
import Preloader from "./Preloader/Preloader";
import Modal from "./Modal/Modal";

import { connect } from 'react-redux';
import * as preloaderActions from './../../redux-preloader/actions';
import * as messagesActions from './../../redux-messages/actions';
import * as editModalActions from '../../redux-editModal/actions';
import * as changeMessageIdActions from './../../redux-change-message-id/actions';

const myUserId = "my-id";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isChanging: { value: false, id: undefined } };
    this.onCreateMessage = this.onCreateMessage.bind(this);
    this.onChangeMessage = this.onChangeMessage.bind(this);
    this.onDeleteMessage = this.onDeleteMessage.bind(this);
    this.onLikeMessage = this.onLikeMessage.bind(this);
    this.onSetChangeMessage = this.onSetChangeMessage.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.isLoaded = false;
  }

  getCurrentDate() {
    const dateMs = new Date();
    const dateStr = dateMs.toISOString();
    return dateStr;
  }

  onDeleteMessage(id) { 
    this.props.deleteMessage(id);
  }

  onLikeMessage(id) {
    this.props.likeMessage(id);
  }

  onCreateMessage(text) {
    this.props.addMessage({
      avatar:
      "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
    createdAt: this.getCurrentDate(),
    editedAt: "",
    text,
    user: "Yuliia",
    userId: myUserId,
    });
  }

  onChangeMessage(id) {
    this.props.showModal();
    this.props.setChangeMessageId(id);
  }

  onSetChangeMessage(id, text) {
    this.props.updateMessage(id, text, this.getCurrentDate());
  }

  handleKeyPress(event){
    if(event.keyCode === 38) {
      const OwnMessages = this.props.messages.filter((message) => message.userId === myUserId);
      if (OwnMessages.length) {
        const lastOwnMessage = OwnMessages[OwnMessages.length - 1];
        this.onChangeMessage(lastOwnMessage.id);
      }
    }
  }
  componentDidMount(){
    document.addEventListener("keydown", this.handleKeyPress);
  }
  componentWillUnmount(){
    document.removeEventListener("keydown", this.handleKeyPress);
  }

  render() {
    if (!this.isLoaded) {
      this.isLoaded = true; 
      fetch(this.props.url)
        .then((response) => {
          return response.json();
        })
        .then((messages) => {
          messages.forEach((message) => {
              this.props.addMessage(message);
          });
          this.props.hidePreloader();
        })
    }
    return (
      <div className="chat">
        {this.props.preloader ? (
          <Preloader />
        ) : (
          <div className="chat-components-wrapper">
            <Header
            messages={this.props.messages}
            />
            <MessageList
              messages={this.props.messages}
              myUserId={myUserId}
              resetChengingFlag={this.resetChengingFlag}
              onDeleteMessage={this.onDeleteMessage}
              onLikeMessage={this.onLikeMessage}
              onChangeMessage={this.onChangeMessage}
            />
            <MessageInput
              onCreateMessage={this.onCreateMessage}
            />
            <Modal changeMessage={this.props.messages.find((message) => message.id === this.props.changeMessageId)} isShown={this.props.editModal} onSetChangeMessage={this.onSetChangeMessage} setChangeMessageId={this.props.setChangeMessageId} hideModal={this.props.hideModal}/>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        preloader: state.preloader,
        messages: state.messages,
        editModal: state.editModal,
        changeMessageId: state.changeMessageId
    }
};

const mapDispatchToProps = {
    ...preloaderActions, ...messagesActions, ...editModalActions, ...changeMessageIdActions
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
