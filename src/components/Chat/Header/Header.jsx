import React from "react";
import "./Header.css";

class Header extends React.Component {
  getStringDate(time) {
    return String(time).length === 1 ? `0${time}` : time;
  }

  render() {
    const users = [];
    this.props.messages.forEach((message) => {
      if (!users.includes(message.user)) {
        users.push(message.user);
      }
    });
    let lastMessage;
    if (this.props.messages.length) {
      lastMessage = this.props.messages[this.props.messages.length - 1];
      const date = new Date(lastMessage.createdAt);
      this.day = this.getStringDate(date.getDate());
      this.month = this.getStringDate(date.getMonth() + 1);
      this.year = date.getFullYear();
      this.hours = this.getStringDate(date.getHours());
      this.minutes = this.getStringDate(date.getMinutes());
    }

    return (
      <div className="header">
        <div className="header-info-wrapper">
          <h1 className="header-title">My chat</h1>
          <span className="header-users-count">
            {users.length + " "} participants
          </span>
          <span className="header-messages-count">
            {this.props.messages.length}
          </span>
          <span>messages</span>
        </div>
        <span className="header-last-message-date">
          {lastMessage
            ? `${this.day}.${this.month}.${this.year} ${this.hours}:${this.minutes}`
            : ""}
        </span>
      </div>
    );
  }
}

export default Header;
