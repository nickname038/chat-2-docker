import React from "react";
import "./Modal.css";

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.onSetChangeMessage = this.onSetChangeMessage.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onClickOk = this.onClickOk.bind(this);
    this.onClickCancel = this.onClickCancel.bind(this);
    this.state = { messageText: "" };
  }

  onSetChangeMessage() {
    this.props.onSetChangeMessage();
  }

  onChange(e) {
    const value = e.target.value;
    this.setState({ messageText: value });
  }

  onClickOk() {
    const text = this.state.messageText || this.props.changeMessage.text;
    if (text) {
      this.props.onSetChangeMessage(this.props.changeMessage.id, text);
      this.props.hideModal();
      this.props.setChangeMessageId(null);
    }
  }

  onClickCancel() {
    this.props.setChangeMessageId(null);
    this.props.hideModal();
    this.setState({ messageText: "" });
  }

  render() {
    return (
      <div
        className={`edit-message-modal${
          this.props.isShown ? " modal-shown" : ""
        }`}
      >
        <div className="modal-wrapper">
          <h1 className="modal-title">Edit message</h1>
          <textarea
            className="edit-message-input"
            value={this.state.messageText || this.props.changeMessage?.text}
            onChange={(e) => this.onChange(e)}
          ></textarea>
          <div className="buttons-wrapper">
            <button
              type="button"
              className="edit-message-button"
              onClick={this.onClickOk}
            >
              OK
            </button>
            <button
              type="button"
              className="edit-message-close"
              onClick={this.onClickCancel}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
